import React from 'react';
import './App.css';
import axios from 'axios';
import PostsList from '../../components/PostsList/PostsList';
import PostForm from '../../components/PostForm/PostForm';

class App extends React.Component {
  state = {
    id: 0,
    title: '',
    body: '',
    posts: [] as any
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/posts')
      .then(res => {
        let newPosts = res.data.slice(0, 5);
        this.setState({
          id: newPosts[newPosts.length - 1].id + 1,
          posts: newPosts
        }, () => console.log(this.state.posts))
        console.log(newPosts)
      })
      .catch(err => console.log("Couldn't fetch data. Error: " + err))
  }

  render() {
    return (
      <div className='ArticleContainer'>
        <h1>Simple blog with React</h1>
        <PostForm id={this.state.id} title={this.state.title} body={this.state.body} changeIdHandler={this.changeId} changeTitleHandler={this.changeTitle} changeBodyHandler={this.changeBody} submitHandler={this.addPost} />
        <PostsList posts={this.state.posts} />
      </div>
    )
  }

  changeId = (event: React.FormEvent<HTMLInputElement>) => {
    const target = event.target as HTMLInputElement;
    let id = target.value;
    this.setState({
      id: id
    })
  }

  changeTitle = (event: React.FormEvent<HTMLInputElement>) => {
    const target = event.target as HTMLInputElement;
    let title = target.value;
    this.setState({
      title: title
    })
  }

  changeBody = (event: React.FormEvent<HTMLTextAreaElement>) => {
    const target = event.target as HTMLTextAreaElement;
    let body = target.value;
    this.setState({
      body: body
    })
  }

  addPost = (event: React.FormEvent<HTMLInputElement>) => {
    event.preventDefault();
    if (this.state.title === '' || this.state.body === '' || this.state.id === 0) {
      alert('No field should be empty');
      return;
    } else if (this.state.id > this.state.posts.length + 1) {
      alert('Please use the next id');
    } else {
      if (this.state.posts[this.state.id - 1] === undefined) {
        axios.post("https://jsonplaceholder.typicode.com/posts", {
          id: this.state.id + 1,
          title: this.state.title,
          body: this.state.body
        })
          .then(res => {
            console.log(res);
            let newPost = res.data;
            let newPosts = [...this.state.posts, newPost];
            this.setState({
              id: this.state.id + 1,
              title: '',
              body: '',
              posts: newPosts
            });
          })
          .catch(err => console.log(err));
      }
    }
  }
}

export default App;