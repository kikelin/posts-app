import React, {FC} from 'react'

type Props = {
    id: number,
    title: string,
    body:string,
    changeTitleHandler(event: React.FormEvent<HTMLInputElement>): void,
    changeBodyHandler(event: React.FormEvent<HTMLTextAreaElement>): void,
    changeIdHandler(event: React.FormEvent<HTMLInputElement>): void,
    submitHandler(event: React.FormEvent<HTMLInputElement>):void
}

const PostForm: FC<Props> = ({ id, title, body, changeIdHandler , changeTitleHandler, changeBodyHandler, submitHandler}) => {
    return (
        <div className='AddArticle'>
            <b>id of article: </b>
            <input type='number' value={id} onChange={changeIdHandler} />
            <form>
                <input type='text' placeholder='Title' value={title} onChange={changeTitleHandler} />
                <textarea placeholder='Enter Body' value={body} onChange={changeBodyHandler}>
                </textarea>
                <input type='submit' value='Add' onClick={submitHandler} />
            </form>
        </div>
    )
};

export default PostForm