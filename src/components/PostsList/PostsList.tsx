import React, { FC } from 'react'

type Props = {
    posts: []
}

const PostsList: FC<Props> = ({ posts }) => {
    return (
        <div>
            {
                posts.map((post: { title: string; body: string; }, index) => (
                    <article key={index}>
                        <h2>{index + 1}. {post.title}</h2>
                        <p>{post.body!.substr(0, 100)}...</p>
                        <button className='delete'>Delete</button>
                        <button className='edit'>Edit</button>
                    </article>
                ))
            }
        </div>
    )
};

export default PostsList
